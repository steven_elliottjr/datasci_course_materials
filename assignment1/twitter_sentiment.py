import json
import sys


def sent_dict(s_file):
    s_dict = []  # empty dict
    for sentiment in s_file.readlines():
        line = sentiment.split("\t")
        s_dict.append((line[0], line[1]))
    return s_dict


def go(decoded_txt, fp):
    for line in decoded_txt:
        float_x = 0.0
        for (x, y) in fp:
            if ((x + " " )  or (" " + x)) in line:
                float_x += float(y)
        print line + "  : " + str(float_x)


def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    x = test_it(sent_file, tweet_file)
    y = sent_dict(sent_file)
    go(x, y)


def test_it(s_file, t_file):
    coded = []
    decoded_txt = []
    for x in t_file.readlines():
        y = json.loads(x)
        if y.has_key("text"):
            coded.append(y["text"])

    for x in coded:
        decoded_txt.append((x.encode("utf-8")))
    return decoded_txt


if __name__ == '__main__':
    main()